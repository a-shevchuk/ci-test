FROM ubuntu:22.04

ENV BASE_DIR="/opt"
ARG DEBIAN_FRONTEND="noninteractive"
WORKDIR "$BASE_DIR"

RUN apt update && \
    apt install --no-install-recommends -y \
        ca-certificates \
        wget && \
    apt clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN touch "$(date +'%Y.%m.%d-%H.%M.%S')"

COPY toolchain toolchain
